package com.example.rxjavaintroductionapp

import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET("posts")
    fun getMessages() : Call<List<Message>>
}