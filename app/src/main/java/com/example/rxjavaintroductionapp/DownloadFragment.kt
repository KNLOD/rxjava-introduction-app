package com.example.rxjavaintroductionapp

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.inflate
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.rxjavaintroductionapp.DownloadFragment.Companion.BASE_URL
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.nio.file.Files.find

class DownloadFragment : Fragment() {
    companion object{
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_download, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val localDataButton : Button = view.findViewById(R.id.local_data_button)
        val restDataButton : Button = view.findViewById(R.id.rest_data_button)

        val viewModel : ObserverViewModel by activityViewModels()

        localDataButton.setOnClickListener{
            val messagesInputStream = resources.openRawResource(R.raw.messages)
            val observable = localDatasource(messagesInputStream)
            viewModel.setObservable(observable)
        }


        restDataButton.setOnClickListener{
            val observable = remoteDatasource()
            viewModel.setObservable(observable)

        }
    }

    }
    fun localDatasource(inputStream : InputStream) : Observable<Message>{
        return Observable.create{ subscriber ->
            // Read local JSON from inputStream
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            var line = bufferedReader.readLine()
            while (line != null) {
                stringBuilder.append(line).append("\n")
                line = bufferedReader.readLine()
            }
            bufferedReader.close()
            // Convert given result string to Object instance
            val messages_string = stringBuilder.toString()
            val messagesList = Gson().fromJson(messages_string, ListMessages::class.java)
            // Notify observer/subscriber
            for (message in messagesList.messages){
                Thread.sleep(500)
                subscriber.onNext(message)
            }
            // Notify that it is completed
            subscriber.onComplete()
        }
    }
    fun remoteDatasource() : Observable<Message>{
        return Observable.create{ subscriber ->
            // retrofit builder
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(ApiInterface::class.java)

            // execute request
            val retrofitData = retrofitBuilder.getMessages().execute()
            val messages = retrofitData.body()

            // notify observer
            messages?.let {
                for (message in messages) {
                    Thread.sleep(500)
                    subscriber.onNext(message)
                }
                // notify observer that process is completed
                subscriber.onComplete()
            }

    }
}

