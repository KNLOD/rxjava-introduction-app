package com.example.rxjavaintroductionapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.core.Observable

class ObserverViewModel : ViewModel() {

    private var _observable = MutableLiveData<Observable<Message>>()
    val observable : LiveData<Observable<Message>>
    get() = _observable

    fun setObservable(observable : Observable<Message>){
        _observable.value = observable
    }
}