package com.example.rxjavaintroductionapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class SubscribeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_subscribe, container, false)
        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val startButton : Button = view.findViewById(R.id.start_button)
        val messageText : TextView = view.findViewById(R.id.data_status)
        startButton.isEnabled = false
        val viewModel : ObserverViewModel by activityViewModels()

        lateinit var observable : Observable<Message>

        viewModel.observable.observe(viewLifecycleOwner, Observer{
            observable = it
            startButton.isEnabled = true
        })

        startButton.setOnClickListener{
            observable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    messageText.text = it.title.toString()

                }, {
                    messageText.text = "Error"
                }, {
                    messageText.text="That's All"
                }
                )

        }

        }



        }